#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>

#include <cstdint>
#include <iostream>

template <typename _CountofType, size_t _SizeOfArray>
char (*__countof_helper( _CountofType (&_Array)[_SizeOfArray]))[_SizeOfArray];
#define _countof(_Array) sizeof(*__countof_helper(_Array))

#define SL_LIDAR_RESP_MEASUREMENT_QUALITY_SHIFT  2

#define SL_LIDAR_VARBITSCALE_X2_SRC_BIT  9
#define SL_LIDAR_VARBITSCALE_X4_SRC_BIT  11
#define SL_LIDAR_VARBITSCALE_X8_SRC_BIT  12
#define SL_LIDAR_VARBITSCALE_X16_SRC_BIT 14

#define SL_LIDAR_VARBITSCALE_X2_DEST_VAL 512
#define SL_LIDAR_VARBITSCALE_X4_DEST_VAL 1280
#define SL_LIDAR_VARBITSCALE_X8_DEST_VAL 1792
#define SL_LIDAR_VARBITSCALE_X16_DEST_VAL 3328

typedef struct _sl_lidar_response_ultra_cabin_nodes_t
{
    // 31                                              0
    // | predict2 10bit | predict1 10bit | major 12bit |
    // std::uint32_t combined_x3;
    std::uint32_t major;
    std::uint32_t predict1;
    std::uint32_t predict2;
} __attribute__((packed)) ultra_cabin_t;

typedef struct _sl_lidar_response_ultra_capsule_measurement_nodes_t
{
    std::uint8_t                             s_checksum_1; // see [s_checksum_1]
    std::uint8_t                             s_checksum_2; // see [s_checksum_1]
    std::uint16_t                            start_angle_sync_q6;
    ultra_cabin_t  ultra_cabins[32];
} __attribute__((packed)) ultra_capsule_t;

typedef struct sl_lidar_response_measurement_node_hq_t
{
    std::uint16_t   angle_z_q14;
    std::uint32_t   dist_mm_q2;
    std::uint8_t    quality;
    std::uint8_t    flag;
} __attribute__((packed)) measurement_hq_t;


static std::uint32_t _varbitscale_decode(std::uint32_t scaled, std::uint32_t & scaleLevel)
{
    static const std::uint32_t VBS_SCALED_BASE[] = {
	SL_LIDAR_VARBITSCALE_X16_DEST_VAL,
	SL_LIDAR_VARBITSCALE_X8_DEST_VAL,
	SL_LIDAR_VARBITSCALE_X4_DEST_VAL,
	SL_LIDAR_VARBITSCALE_X2_DEST_VAL,
	0,
    };

    static const std::uint32_t VBS_SCALED_LVL[] = {
	4,
	3,
	2,
	1,
	0,
    };

    static const std::uint32_t VBS_TARGET_BASE[] = {
	(0x1 << SL_LIDAR_VARBITSCALE_X16_SRC_BIT),
	(0x1 << SL_LIDAR_VARBITSCALE_X8_SRC_BIT),
	(0x1 << SL_LIDAR_VARBITSCALE_X4_SRC_BIT),
	(0x1 << SL_LIDAR_VARBITSCALE_X2_SRC_BIT),
	0,
    };

    for (size_t i = 0; i < _countof(VBS_SCALED_BASE); ++i) {
	int remain = ((int)scaled - (int)VBS_SCALED_BASE[i]);
	if (remain >= 0) {
	    scaleLevel = VBS_SCALED_LVL[i];
	    return VBS_TARGET_BASE[i] + (remain << scaleLevel);
	}
    }
    return 0;
}

void _puc_process(const ultra_capsule_t &capsule, const ultra_capsule_t &prev_capsule, measurement_hq_t *nodebuffer, size_t &nodeCount)
{
    nodeCount = 0;

    int diffAngle_q8;
    int currentStartAngle_q8 = ((capsule.start_angle_sync_q6 & 0x7FFF) << 2);
    int prevStartAngle_q8 = ((prev_capsule.start_angle_sync_q6 & 0x7FFF) << 2);

    diffAngle_q8 = (currentStartAngle_q8)-(prevStartAngle_q8);
    if (prevStartAngle_q8 > currentStartAngle_q8) {
	diffAngle_q8 += (360 << 8);
    }

    int angleInc_q16 = (diffAngle_q8 << 3) / 3;
    int currentAngle_raw_q16 = (prevStartAngle_q8 << 8);
    for (size_t pos = 0; pos < _countof(prev_capsule.ultra_cabins); ++pos) {

	int dist_q2[3];
	int angle_q6[3];
	int syncBit[3];


	// unpack ...
	int dist_major = prev_capsule.ultra_cabins[pos].major;

	// signed partical integer, using the magic shift here
	// DO NOT TOUCH

	int dist_predict1 = prev_capsule.ultra_cabins[pos].predict1;
	int dist_predict2 = prev_capsule.ultra_cabins[pos].predict2;

	int dist_major2;

	std::uint32_t scalelvl1, scalelvl2;

	if (pos == _countof(prev_capsule.ultra_cabins) - 1) {
	    dist_major2 = (capsule.ultra_cabins[0].major);
	}
	else {
	    dist_major2 = (prev_capsule.ultra_cabins[pos + 1].major);
	}

	// decode with the var bit scale ...
	dist_major = _varbitscale_decode(dist_major, scalelvl1);
	dist_major2 = _varbitscale_decode(dist_major2, scalelvl2);


	int dist_base1 = dist_major;
	int dist_base2 = dist_major2;

	if ((!dist_major) && dist_major2) {
	    dist_base1 = dist_major2;
	    scalelvl1 = scalelvl2;
	}

    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wsign-compare"

	dist_q2[0] = (dist_major << 2);
	if ((dist_predict1 == 0xFFFFFE00) || (dist_predict1 == 0x1FF)) {
	    dist_q2[1] = 0;
	}
	else {
	    dist_predict1 = (dist_predict1 << scalelvl1);
	    dist_q2[1] = (dist_predict1 + dist_base1) << 2;

	}

	if ((dist_predict2 == 0xFFFFFE00) || (dist_predict2 == 0x1FF)) {
	    dist_q2[2] = 0;
	}

    #pragma GCC diagnostic pop

	else {
	    dist_predict2 = (dist_predict2 << scalelvl2);
	    dist_q2[2] = (dist_predict2 + dist_base2) << 2;
	}


	for (int cpos = 0; cpos < 3; ++cpos) {
	    syncBit[cpos] = (((currentAngle_raw_q16 + angleInc_q16) % (360 << 16)) < angleInc_q16) ? 1 : 0;

	    int offsetAngleMean_q16 = (int)(7.5 * 3.1415926535 * (1 << 16) / 180.0);

	    if (dist_q2[cpos] >= (50 * 4))
	    {
		const int k1 = 98361;
		const int k2 = int(k1 / dist_q2[cpos]);

		offsetAngleMean_q16 = (int)(8 * 3.1415926535 * (1 << 16) / 180) - (k2 << 6) - (k2 * k2 * k2) / 98304;
	    }

	    angle_q6[cpos] = ((currentAngle_raw_q16 - int(offsetAngleMean_q16 * 180 / 3.14159265)) >> 10);
	    currentAngle_raw_q16 += angleInc_q16;

	    if (angle_q6[cpos] < 0) angle_q6[cpos] += (360 << 6);
	    if (angle_q6[cpos] >= (360 << 6)) angle_q6[cpos] -= (360 << 6);

	    measurement_hq_t node;

	    node.flag = (syncBit[cpos] | ((!syncBit[cpos]) << 1));
	    node.quality = dist_q2[cpos] ? (0x2F << SL_LIDAR_RESP_MEASUREMENT_QUALITY_SHIFT) : 0;
	    node.angle_z_q14 = std::uint16_t((angle_q6[cpos] << 8) / 90);
	    node.dist_mm_q2 = dist_q2[cpos];

	    nodebuffer[nodeCount++] = node;
	}

    }
}


ultra_capsule_t *pyobject_to_ultra_capsule(PyObject *o) {

    ultra_capsule_t *ultra_capsule = new ultra_capsule_t();

    PyObject *start_angle_q6_o = PyObject_GetAttrString(o, "start_angle_q6");
    ultra_capsule->start_angle_sync_q6 = (std::uint16_t) PyLong_AsLong(start_angle_q6_o);
    Py_DECREF(start_angle_q6_o);

    PyObject *ultra_cabins_o = PyObject_GetAttrString(o, "ultra_cabins");
    for (Py_ssize_t i = 0; i < PyList_Size(ultra_cabins_o); i++) {
	PyObject *cabin_o = PyList_GetItem(ultra_cabins_o, i);

	PyObject *major_o = PyObject_GetAttrString(cabin_o, "major");
	std::uint32_t major = (std::uint32_t) PyLong_AsLong(major_o);
	Py_DECREF(major_o);

	PyObject *predict1_o = PyObject_GetAttrString(cabin_o, "predict1");
	std::uint32_t predict1 = (std::uint32_t) PyLong_AsLong(predict1_o);
	Py_DECREF(predict1_o);

	PyObject *predict2_o = PyObject_GetAttrString(cabin_o, "predict2");
	std::uint32_t predict2 = (std::uint32_t) PyLong_AsLong(predict2_o);
	Py_DECREF(predict2_o);

	ultra_capsule->ultra_cabins[i].major = major;
	ultra_capsule->ultra_cabins[i].predict1 = predict1;
	ultra_capsule->ultra_cabins[i].predict2 = predict2;
    }

    return ultra_capsule;
}

typedef struct {
    PyObject_HEAD
    int start_flag;
    int quality;
    int angle_z_q14;
    int dist_mm_q2;
} PyRPlidarUltraMeasurementHQ;

static PyMemberDef PyRPlidarUltraMeasurementHQ_members[] = {
    {"start_flag", T_INT, offsetof(PyRPlidarUltraMeasurementHQ, start_flag), 0,
     "start flag"},
    {"quality", T_INT, offsetof(PyRPlidarUltraMeasurementHQ, quality), 0,
     "quality"},
    {"angle_z_q14", T_INT, offsetof(PyRPlidarUltraMeasurementHQ, angle_z_q14), 0,
     "angle"},
    {"dist_mm_q2", T_INT, offsetof(PyRPlidarUltraMeasurementHQ, dist_mm_q2), 0,
     "distance"},
    {NULL}  /* Sentinel */
};



#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wc99-designator"

static PyTypeObject PyRPlidarUltraMeasurementHQType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "puc.PyRPlidarUltraMeasurementHQ",
    .tp_basicsize = sizeof(PyRPlidarUltraMeasurementHQ),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_doc = PyDoc_STR("a PyRPlidarUltraMeasurementHQ"),
    .tp_members = PyRPlidarUltraMeasurementHQ_members,
    .tp_new = PyType_GenericNew
};

#pragma GCC diagnostic pop

static PyObject*
puc_process(PyObject *self, PyObject *args) {

    // PyObject *capsule_prev, *capsule_current;
    PyObject *capsule_prev, *capsule_current;

    if (!PyArg_ParseTuple(args, "OO", &capsule_prev, &capsule_current)) {
	return NULL;
    }

    ultra_capsule_t *ultra_capsule_prev = pyobject_to_ultra_capsule(capsule_prev);
    ultra_capsule_t *ultra_capsule_current = pyobject_to_ultra_capsule(capsule_current);

    measurement_hq_t measurement[96];
    size_t measurement_nb = 0;
    _puc_process(*ultra_capsule_current, *ultra_capsule_prev, measurement, measurement_nb);

    delete ultra_capsule_prev;
    delete ultra_capsule_current;

    PyObject *list = PyList_New(0);

    for (int i = 0; i < 96; i++) {
	
	PyRPlidarUltraMeasurementHQ* measurement_hq = PyObject_New(PyRPlidarUltraMeasurementHQ, &PyRPlidarUltraMeasurementHQType);
	measurement_hq->start_flag = measurement[i].flag;
	measurement_hq->quality = measurement[i].quality;
	measurement_hq->angle_z_q14 = measurement[i].angle_z_q14;
	measurement_hq->dist_mm_q2 = measurement[i].dist_mm_q2;

	PyList_Append(list, (PyObject *) measurement_hq);

	Py_DECREF(measurement_hq);
    }

    return list;
}

static PyMethodDef PUCMethods[] = {
    {"process",  puc_process, METH_VARARGS,
     "process an ultra capsule"},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};

static struct PyModuleDef pucmodule = {
    PyModuleDef_HEAD_INIT,
    "puc",
    NULL,
    -1,
    PUCMethods
};

PyMODINIT_FUNC
PyInit_puc(void)
{
    if (PyType_Ready(&PyRPlidarUltraMeasurementHQType) < 0) {
        return NULL;
    }

    PyObject *module = PyModule_Create(&pucmodule);
    if (module == NULL) {
	return NULL;
    }

    Py_INCREF(&PyRPlidarUltraMeasurementHQType);
    if (PyModule_AddObject(module, "PyRPlidarUltraMeasurementHQ", (PyObject*)&PyRPlidarUltraMeasurementHQType) < 0) {
	Py_DECREF(&PyRPlidarUltraMeasurementHQType);
        Py_DECREF(module);
        return NULL;
    }

    return module;
}
