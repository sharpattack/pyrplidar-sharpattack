def __bootstrap__():
    global __bootstrap__, __loader__, __file__
    import sys
    import imp
    from importlib.resources import files

    libs = []
    for file in files(sys.modules[__package__]).iterdir():
        name = file.name
        if name.endswith(".so"):
            libs.append(file)

    if len(libs) == 0:
        raise ImportError("Could not find any compiled library")

    errors = []
    for lib in libs:
        try:
            imp.load_dynamic(__name__, str(lib))
            __file__ = str(lib)
            __loader__ = None
            del __bootstrap__, __loader__
            return
        except ImportError as e:
            errors.append(e)
    raise ImportError(
        f"None of the compiled library in {libs} is compatible with your current python version\n Collected errors {errors}"
    )


__bootstrap__()
