# build.py
from typing import Any, Dict
from pybind11.setup_helpers import Pybind11Extension, build_ext


def build(setup_kwargs: Dict[str, Any]) -> None:
    ext_modules = [
        Pybind11Extension("puc.lib", ["puc/puc.cpp"]),
    ]

    setup_kwargs.update(
        {
            "ext_modules": ext_modules,
            "cmd_class": {"build_ext": build_ext},
            "zip_safe": False,
        }
    )
